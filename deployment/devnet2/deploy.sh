#!/bin/bash

# 1. Email input validation
echo -e "\033[1;33m### Starting Filehub deployment to devnet2 ###\033[0m"
echo "Enter your Chromaway email to associate deployment: "
read EMAIL
if [ -z "${EMAIL}" ]
then
    echo -e "\033[1;31mEmail must be supplied for deployment!\033[0m"
    exit 1
fi

# 2. Obtain BRID
echo -e "\033[1;33m### Obtain devnet2 Directory chain BRID ###\033[0m"
URL=https://node0.devnet2.chromia.dev:7740
BRID=$(curl "${URL}/brid/iid_0" -s)
echo "Directory chain Brid (devnet2): ${BRID}"

# 3. Provider keys input validation
SYS_PROVIDER_CONFIG=".chromia/sysconfig"

if [ ! -f "${SYS_PROVIDER_CONFIG}" ]; then
    echo "Enter system provider pubkey: "
    read SYSTEM_PROVIDER_PUB_KEY

    if [ -z "${SYSTEM_PROVIDER_PUB_KEY}" ]
    then
        echo -e "\033[1;31mSystem Provider PubKey must be supplied!\033[0m"
        exit 1
    fi

    echo "Enter system provider privkey: "
    read SYSTEM_PROVIDER_PRIV_KEY

    if [ -z "${SYSTEM_PROVIDER_PRIV_KEY}" ]
    then
        echo -e "\033[1;31mSystem Provider PrivKey must be supplied!\033[0m"
        exit 1
    fi

cat > ${SYS_PROVIDER_CONFIG} <<- EOM
pubkey = ${SYSTEM_PROVIDER_PUB_KEY}
privkey = ${SYSTEM_PROVIDER_PRIV_KEY}
api.url = ${URL}
brid = ${BRID}
EOM
fi
echo ""

# 4. Generate deployment keys
echo -e "\033[1;33m### Generating deployment keys ###\033[0m"
SECRET_FILE_NAME=".secret.deploy-devnet2"

if [ ! -f "${SECRET_FILE_NAME}" ]; then
    echo "Creating new key pair"
    chr keygen -s ${SECRET_FILE_NAME}
else
    echo -e "\033[1;32mDeployment key pair already exists. Reusing.\033[0m"
fi
echo ""

# 5. Register deployment keys (Community node provider)
echo -e "\033[1;33m### Registering deployment keys ###\033[0m"

# 6. Read keys from deployment keygen secret file
. ${SECRET_FILE_NAME}
DEPLOYMENT_PUB_KEY=$pubkey
DEPLOYMENT_PRIV_KEY=$privkey

REGISTRATION_CHECK=$( (pmc provider info --config $SYS_PROVIDER_CONFIG --pubkey $DEPLOYMENT_PUB_KEY) 2> /dev/null)
echo $REGISTRATION_CHECK

if [[ "${REGISTRATION_CHECK}" == *"${DEPLOYMENT_PUB_KEY}"* ]]; then
    echo -e "\033[1;32mCommunity Node Provider already registered\033[0m"
    echo "${REGISTRATION_CHECK}"
else
    echo "Community Node Provider registration for deployment keys"
    pmc provider register --config ${SYS_PROVIDER_CONFIG} \
                          -cnp \
                          --enable \
                          --pubkey ${DEPLOYMENT_PUB_KEY}
fi
echo ""

# # 7. Register container in network
echo -e "\033[1;33m### Registering container in network ###\033[0m"
CONTAINER_NAME="filehub"
CONTAINER_CHECK=$( (pmc container info --config ${SYS_PROVIDER_CONFIG} --name ${CONTAINER_NAME})  2> /dev/null)
echo $CONTAINER_CHECK

if [[ "${CONTAINER_CHECK}" == *"${EMAIL}"* ]]; then
    echo -e "\033[1;32mContainer already registered\033[0m"
    echo "${CONTAINER_CHECK}"
else
    echo "Container registration in network started"
    pmc container add --config ${SYS_PROVIDER_CONFIG} \
                      --cluster system --name ${CONTAINER_NAME} \
                      --pubkeys ${DEPLOYMENT_PUB_KEY}
fi
echo ""

# # 8. Create deployment config
echo -e "\033[1;33m### Creating deployment config ###\033[0m"
DEPLOYMENT_CONFIG=".chromia/config"
cat > $DEPLOYMENT_CONFIG <<- EOM
pubkey = ${DEPLOYMENT_PUB_KEY}
privkey = ${DEPLOYMENT_PRIV_KEY}
api.url = ${URL}
brid = ${BRID}
EOM

if [ -f "$DEPLOYMENT_CONFIG" ]; then
    echo -e "\033[1;32mDeployment config created.\033[0m"
fi
echo ""

# 9. Associate email to deployment key
echo -e "\033[1;33m### Associate email to provider ###\033[0m"
if [[ "${REGISTRATION_CHECK}" != *"${EMAIL}"* ]]; then
    echo "Registering email to provider"
    pmc provider update --config ${SYS_PROVIDER_CONFIG} --name ${EMAIL}
else
    echo -e "\033[1;32mEmail already registered to provider\033[0m"
fi
echo ""

# 10. Update chromia.yml
CHROMIA_FILE="../../chromia.yml"
if ! grep -q $BRID "${CHROMIA_FILE}"; then
echo -e "\033[1;33m### Adding deployment entry to chromia.yml ###\033[0m"
cat <<EOT >> $CHROMIA_FILE
  devnet2:
      url: ${URL}
      brid: x"${BRID}"
      container: ${CONTAINER_NAME}
EOT
echo -e "\033[1;32mProject file chromia.yml updated.\033[0m"
fi
echo ""

# 11. Deploy to devnet2
chr install --settings $CHROMIA_FILE
chr build --settings $CHROMIA_FILE
chr deployment create --network devnet2 \
                      --blockchain filehub \
                      --config $DEPLOYMENT_CONFIG \
                      --secret $SECRET_FILE_NAME \
                      --settings $CHROMIA_FILE


echo ""
echo -e "\033[1;32mDeployment to devnet2 completed.\033[0m"
