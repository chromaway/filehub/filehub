## Start locally

prereq: docker, chromia cli

1. docker compose up -d
2. chr install
3. env CHR_DB_URL=jdbc:postgresql://localhost:5434/filestorage bash -c 'chr node start'

## Start in devnet2

prereq:

- [Chroma CLI](https://docs.chromia.com/getting-started/dev-setup/cli-installation)
- [PMC CLI](https://docs.chromia.com/providers/pmc/pmccli-installation)

Information required before running script:

- System Provider keys to devnet2 (required to register your)
- Your Chromaway email address

1. cd deployment/devnet2
2. ./deploy.sh

## Run Tests (2 options)

1. NPM

   - npm install
   - npm run test

2. Chromia CLI
   - chr install
   - docker compose up -d
   - env CHR_DB_URL=jdbc:postgresql://localhost:5434/filestorage bash -c 'chr test'
