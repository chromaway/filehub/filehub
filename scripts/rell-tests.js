const { spawnSync } = require('child_process');
const path = require('path');
require('dotenv').config();

const rellDirectory = path.join(__dirname, '..', '/rell');
console.log('Executing tests for', rellDirectory);

function verifyDocker() {
  const { status } = spawnSync(`docker ps`, { shell: true });
  console.log('Docker is running!');

  if (status !== 0) {
    console.error('Docker is not running');
    process.exit(1);
  }
}

async function test(config) {
  const currentWorkingDirectory = process.cwd();
  const baseCommand = 'docker run --rm ' + 
                        '--add-host=host.docker.internal:host-gateway ' + 
                        '-e CHR_DB_URL=jdbc:postgresql://host.docker.internal:5432/postchain ' + 
                        '-e CHR_DB_USER=postchain ' + 
                        '-e CHR_DB_PASSWORD=postchain ' + 
                        `-v ${currentWorkingDirectory}:/usr/app ` + 
                        '--name filehub_test_blockchain ' + 
                        'registry.gitlab.com/chromaway/core-tools/chromia-cli/chr:0.20.1 ' + 
                        'chr test --use-db';
  const command = config.tests ? `${baseCommand} --tests ${config.tests}` : baseCommand;

  console.log(command);

  try {
    const { status } = spawnSync(command, { shell: true, stdio: 'inherit' });

    if (status !== 0) {
      console.error('Tests failed');
      process.exit(1);
    }
  } finally {
    spawnSync('npm run stop', { shell: true });
  }
}

async function main() {
  const config = {
    tests: process.env.npm_config_filter,
  };
  verifyDocker();
  await test(config);
}

main();
