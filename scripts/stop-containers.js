const { execSync } = require('child_process');

function stopContainers() {
  try {
    execSync(`docker rm -v -f filehub_test_blockchain`, { encoding: 'utf-8' });
    execSync(`docker rm -v -f filehub_test_postgres`, { encoding: 'utf-8' });
  } catch (error) {
    if (!error.message.includes('No such container')) {
      console.error(`Error stopping and removing containers: ${error.message}`);
      process.exit(1);
    }
  }
}

stopContainers();
