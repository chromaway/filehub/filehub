operation mint_to_dapp(amount: big_integer, to_dapp_brid: byte_array) {
  val account = ft4.auth.authenticate();

  val asset = require(ft4.assets.asset @? { .id == fs.payments_state.filehub_credit_asset_id }, "Filehub credit asset not found");

  var dapp_account = ft4.accounts.account @? { .id == to_dapp_brid };
  if (dapp_account == null) {
    dapp_account = create ft4.accounts.account(to_dapp_brid, "DAPP");
  }

  // Transfer payment asset from user to filehub deposit account
  val payment_asset = fs.get_payment_asset();
  ft4.assets.Unsafe.transfer(account, fs.payments_state.filehub_deposit_account, payment_asset, amount);

  // Mint filehub credits to the dapp account
  ft4.assets.Unsafe.mint(dapp_account, asset, amount);
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = "credit.mint_to_dapp",
    flags = ["T"],
    message = allocate_file_message_handler(*)
  );

function allocate_file_message_handler(gtv) {
  val args = struct<mint_to_dapp>.from_gtv(gtv);
  val amount = args.amount;
  val to_dapp_brid = args.to_dapp_brid;

  return "Login is required to mint credits to dapp.\n[mint_to_dapp] operation called with arguments:\n\n- Amount: %s\n- Dapp BRID: %s\nNonce: {nonce}"
    .format(amount, to_dapp_brid);
}
