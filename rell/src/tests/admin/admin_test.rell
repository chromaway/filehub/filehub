import lib.ft4.test.utils.{ ft_auth_operation_for };

function test_clear_at_init() {
  assert_null(fs.filechain @? { });
  assert_null(fs.active_filechain @? { });
  assert_null(fs.disabled_filechain @? { });
  assert_null(fs.chunk @? { }); // might be excessive from here, or misplaced
  assert_null(fs.chunk_size @? { });
  assert_null(fs.file @? { });
  assert_null(fs.file_chunk @? { });
}

function test_add_filechain() {
  val filechain_owner = test_utils.create_ft4_test_acc(rell.test.keypairs.bob); // create account
  rell.test.tx()
    .op(admin.add_filechain(data.chain1.brid, filechain_owner.id))
    .sign(data.admin_keypair)
    .run(); // test adding with admin
  rell.test.tx()
    .op(admin.add_filechain(data.chain2.brid, filechain_owner.id))
    .sign(data.not_admin_keypair)
    .run_must_fail(); // test adding without admin
  assert_equals((fs.active_filechain @* { .filechain.brid == data.chain1.brid }).size(), 1); // make sure added filechain automatically is active
}

function test_disable_filechain() {
  val filechain_owner = test_utils.create_ft4_test_acc(rell.test.keypairs.bob); // create account
  rell.test.tx()
    .op(admin.add_filechain(data.chain2.brid, filechain_owner.id))
    .sign(data.admin_keypair).run(); // add 
  rell.test.tx()
    .op(admin.disable_filechain(data.chain2.brid))
    .sign(data.not_admin_keypair)
    .run_must_fail(); // test disabling without admin
  rell.test.tx()
    .op(admin.disable_filechain(data.chain2.brid))
    .sign(data.admin_keypair)
    .run(); // test disabling with admin 
  assert_null(fs.active_filechain @? { .filechain.brid == data.chain2.brid }); // make sure it is not still active
  assert_equals((fs.disabled_filechain @* { .filechain.brid == data.chain2.brid }).size(), 1); // make sure filechain is disabled
}

function test_enable_filechain() {
  val filechain_owner = test_utils.create_ft4_test_acc(rell.test.keypairs.bob); // create account
  rell.test.tx()
    .op(admin.add_filechain(data.chain3.brid, filechain_owner.id))
    .sign(data.admin_keypair)
    .run(); // add
  rell.test.tx()
    .op(admin.disable_filechain(data.chain3.brid))
    .sign(data.admin_keypair)
    .run(); // disable
  rell.test.tx()
    .op(admin.enable_filechain(data.chain3.brid))
    .sign(data.not_admin_keypair)
    .run_must_fail(); // enable without admin
  rell.test.tx()
    .op(admin.enable_filechain(data.chain3.brid))
    .sign(data.admin_keypair)
    .run(); // enable with admin
  // make sure filechain is active
  assert_equals((fs.active_filechain @* { .filechain.brid == data.chain3.brid }).size(), 1);
}

function test_delete_file() {
  val auth_data = test_utils.create_ft4_test_acc(rell.test.keypairs.alice); // create account
  rell.test.tx()
    .op(admin.add_filechain(data.chain3.brid, test_utils.create_ft4_test_acc(rell.test.keypairs.bob).id))
    .sign(data.admin_keypair)
    .run(); // add
  rell.test.tx()
    .op(ft_auth_operation_for(rell.test.keypairs.alice.pub))
    .op(fs.allocate_file(data.file1.file_hash, data.file1.total_bytes, data.file1.metadata))
    .sign(auth_data.keypair)
    .run(); // add a file 
  rell.test.tx()
    .op(admin.delete_file(data.file1.file_hash))
    .sign(data.admin_keypair)
    .run(); // delete file 
  assert_equals(fs.get_chunk_locations(data.file1.file_hash), list<fs.chunk_location>()); // make sure there are no chunk locations
}
//could just return tx from these and then run all transactions in one block in the end I think :) 