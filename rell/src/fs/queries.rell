query has_file(file_hash: byte_array) {
  return file @? { .hash == file_hash } != null;
}

query validate_file(file_hash: byte_array) {
  val file = file @ { .hash == file_hash };
  val allocated_size_for_initialized_chunks = ( fc: file_chunk, cs: chunk_size) @ {
    fc.file == file,
    fc.chunk == cs.chunk,
    fc.chunk.initialized
  } ( @sum cs.size );

  return file.allocated_size == allocated_size_for_initialized_chunks;
}

query get_file_metadata(file_hash: byte_array) {
  return file @ { .hash == file_hash } ( .metadata );
}

query get_chunk_locations(file_hash: byte_array) {
  val chunks = ( active: active_filechain, file_chunk) @* {
    file_chunk.file.hash == file_hash,
    file_chunk.filechain == active.filechain
  } (
    @sort idx = file_chunk.idx,
    hash = file_chunk.chunk.hash,
    brid = active.filechain.brid
  );

  val persisted_indexes = set<integer>();
  val filtered = list<chunk_location>();

  for (chunk in chunks) {
    if (not persisted_indexes.contains(chunk.idx)) {
      filtered.add(chunk_location(chunk.idx, hash = chunk.hash, brid = chunk.brid));
      persisted_indexes.add(chunk.idx);
    }
  }

  return filtered;
}

query get_allocated_mb_in_filechain(brid: byte_array) {
  val sizes = chunk_size @* { .chunk.filechain.brid == brid } ( .size );

  var mb = 0.0;
  for (size in sizes) {
    mb += (decimal(size) / decimal(1048576));
  }

  return mb;
}

query get_all_file_hashes(account_id: byte_array) {
  return file @* { .creator.id == account_id } ( .hash );
}

query get_all_files(account_id: byte_array) {
  return file @* { .creator.id == account_id } ( .hash, .metadata );
}

query is_chunk_allocated(hash: byte_array) {
  return chunk @? { hash } != null;
}

query is_chunk_initalized(hash: byte_array) {
  return chunk @? { hash } ( .initialized ) ?: false;
}

query calculate_file_allocation_fees(file_bytes: list<integer>): list<file_allocation_fees> {
  return file_bytes @* {} ( _calculate_file_allocation_fees($) );
}

query payment_asset_id() {
  return payments_state.asset_id;
}